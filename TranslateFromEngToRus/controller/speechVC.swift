//
//  speechVC.swift
//  TranslateFromEngToRus
//
//  Created by Vlad Sys on 11/19/17.
//  Copyright © 2017 Vlad Sys. All rights reserved.
//

import UIKit
import Speech
import ROGoogleTranslate

public class speechVC: UIViewController, SFSpeechRecognizerDelegate, UITableViewDelegate, UITableViewDataSource{
    
    var array = [String]()

    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    
    private var recognitionTask: SFSpeechRecognitionTask?
    
    private let audioEngine = AVAudioEngine()
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textFieldOne: UITextField!
    @IBOutlet weak var translatedBtn: UIButton!
    @IBOutlet weak var translateBtn: UIButton!
    public override func viewDidLoad() {
        super.viewDidLoad()

        
          self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //btn
        translateBtn.layer.cornerRadius = 34.5
        translateBtn.layer.borderWidth = 2
        translateBtn.layer.borderColor = UIColor.white.cgColor
        translateBtn.clipsToBounds = true
        
        
        
        translatedBtn.layer.cornerRadius = 34.5
        translatedBtn.layer.borderWidth = 2
        translatedBtn.layer.borderColor = UIColor.white.cgColor
        translatedBtn.clipsToBounds = true
        
        
        // Disable the record buttons until authorization has been granted.
        translateBtn.isEnabled = false
    }

    override public func viewDidAppear(_ animated: Bool) {
        speechRecognizer.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { authStatus in
            /*
             The callback may not be called on the main thread. Add an
             operation to the main queue to update the record button's state.
             */
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.translateBtn.isEnabled = true
                    
                case .denied:
                    self.translateBtn.isEnabled = false
                    self.translateBtn.setTitle("User denied access to speech recognition", for: .disabled)
                    
                case .restricted:
                    self.translateBtn.isEnabled = false
                    self.translateBtn.setTitle("Speech recognition restricted on this device", for: .disabled)
                    
                case .notDetermined:
                    self.translateBtn.isEnabled = false
                    self.translateBtn.setTitle("Speech recognition not yet authorized", for: .disabled)
                }
            }
        }
    }
    
    private func startRecording() throws {
        
        // Cancel the previous task if it's running.
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSessionCategoryRecord)
        try audioSession.setMode(AVAudioSessionModeMeasurement)
        try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
         let inputNode = audioEngine.inputNode
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }
        
        // Configure request so that results are returned before audio recording is finished
        recognitionRequest.shouldReportPartialResults = true
        
        // A recognition task represents a speech recognition session.
        // We keep a reference to the task so that it can be cancelled.
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false
            
            if let result = result {
                self.textFieldOne.text = result.bestTranscription.formattedString
                isFinal = result.isFinal
                print(result.bestTranscription.formattedString)
            }
            
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.translateBtn.isEnabled = true
                self.translateBtn.setImage(UIImage(named: "microImg.png" ), for: [])
            }
        }
        
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        try audioEngine.start()
        
        textFieldOne.text = ""
        
    }
    
    // MARK: SFSpeechRecognizerDelegate
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            translateBtn.isEnabled = true
            translateBtn.setImage(UIImage(named: "microImg.png" ), for: [])
        } else {
            translateBtn.isEnabled = false
            
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return array.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = array[indexPath.row]
        return cell
    }
    
    @IBAction func recordBtnTapped(_ sender: AnyObject) {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            translateBtn.isEnabled = false
            translateBtn.setImage(UIImage(named: "microImg.png" ), for: [])
            tableView.beginUpdates()
            array.append(textFieldOne.text!)
            tableView.insertRows(at: [IndexPath.init(row: array.count-1, section: 0)], with: .fade)
            tableView.endUpdates()
            let translator = ROGoogleTranslate()
            translator.apiKey = "AIzaSyDfyDTvTGMFO9xQr88qbPJbYBHkMl9a6yA" // Add your API Key here
            
            var params = ROGoogleTranslateParams()
            params.source =  "en"
            params.target =  "ru"
            params.text = textFieldOne.text ?? "The textfield is empty"
            
            translator.translate(params: params) { (result) in
                DispatchQueue.main.async {
                    self.tableView.beginUpdates()
                    self.array.append(result)
                    self.tableView.insertRows(at: [IndexPath.init(row: self.array.count-1, section: 0)], with: .fade)
                    self.tableView.endUpdates()
                    
                }
            }
        } else {
            try! startRecording()
            
        }
    }
    
}
